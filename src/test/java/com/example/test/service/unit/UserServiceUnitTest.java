package com.example.test.service.unit;

import com.example.test.service.UserService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceUnitTest {

    UserService service = new UserService();

    @Test
    void createUserTest() {
        assertEquals("User acp2p created.", service.createUser("acp2p"));
    }

    @Test
    void createUserWithEmptyStringTest() {
        assertEquals("User  created.", service.createUser(new String()));
    }

}
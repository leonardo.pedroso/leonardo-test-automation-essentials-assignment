package com.example.test.controller.unit;

import com.example.test.controller.UserController;
import com.example.test.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class UserControllerUnitTest {

    @Test
    void createUserTest() {
        UserService service = Mockito.mock(UserService.class);
        UserController controller = new UserController(service);
        when(service.createUser(anyString())).thenReturn("User admin@admin created.");
        assertEquals("User admin@admin created.", controller.createUser("admin@admin"));
    }
}
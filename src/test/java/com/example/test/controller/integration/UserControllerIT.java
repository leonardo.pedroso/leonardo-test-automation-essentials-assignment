package com.example.test.controller.integration;

import com.example.test.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class UserControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService service;

    @Test
    void shouldGetDefaultCreateUserValueIT() throws Exception {
        when(service.createUser("test")).thenReturn("User test created.");
        mockMvc.perform(get("/create"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("User test created.")));
        verify(service).createUser("test");
    }

    @Test
    void shouldGetCreateUserValueIT() throws Exception {
        when(service.createUser("acp2p.com")).thenReturn("User acp2p.com created.");
        mockMvc.perform(get("/create").param("email", "acp2p.com"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("User acp2p.com created.")));
        verify(service).createUser("acp2p.com");
    }
}

package com.example.test.bdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ValidatableResponse;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.*;

public class UserWasCreatedSD {

    @LocalServerPort
    private int port;
    private String email;
    private ValidatableResponse response;

    @Given("user email is (.+)$")
    public void user_email_is(String email) {
        this.email = email;
        System.out.println("Email: " + email);
    }

    @When("http request to /create")
    public void http_request_to_create() {
        this.response = given().port(this.port).when().request("GET", "/create?email="+this.email).then();
    }

    @Then("http response status code is 200")
    public void http_response_status_code_is_200() {
        this.response.statusCode(200);
    }
}

package com.example.test.service;

import org.springframework.stereotype.Service;

@Service
public class UserService {

    public String createUser(String email) {
        return String.format("User %s created.", email);
    }

}
